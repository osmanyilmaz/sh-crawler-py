#!/usr/bin/env python
# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.common import exceptions
import json
import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
CHROME_PATH = os.path.join(ROOT_DIR, 'chromedriver')
driver = webdriver.Chrome(CHROME_PATH)
adict = {}
if __name__ == '__main__':

    path = '/Users/minimac/Desktop/sahibinden/emlak-turistik_tesis-kiralık-plaj.json'

    driver.get("https://www.sahibinden.com/arama/detayli?category=190466")

    accordions = driver.find_elements_by_xpath("//div[@class='accordion']//dl")
    sum_dict = dict()
    feature_dict = dict()
    attribute_dict = dict()

    for node in accordions:
        try:
            feat_title = node.find_element_by_xpath("dt//a")
            key = feat_title.text
            feature_dict.update({key: []})
            features = node.find_elements_by_xpath("..//dl//dd//div")
            for feature in features:
                text = feature.find_element_by_tag_name("label")
                feature_dict[key].append(text.get_attribute('textContent'))
                # print("-------- " + text.get_attribute('textContent'))
        except exceptions.NoSuchElementException:
            continue
    sum_dict.update({"features": feature_dict})
    elem = driver.find_elements_by_xpath("//table[@class='categorySubTable']//tr")
    for el in elem:
        try:
            key = el.find_element_by_class_name('first')
            if key.text == "Kategori":
                continue

            attribute_dict.update({key.text: []})

            base = el.find_element_by_xpath("td[@class='second']")
            values = base.find_elements_by_xpath(".//option")
            input3 = base.find_elements_by_xpath(".//input")
            text_input = base.find_elements_by_xpath(".//input[@class='inputDefault wordInput']")

            if len(text_input) == 1:
                attribute_dict[key.text].append("text input")
            elif len(input3) == 2:
                attribute_dict[key.text].append("range")
            elif values:
                # all_options = values.find_elements_by_tag_name("option")
                for al in values:
                    attribute_dict[key.text].append(al.get_attribute('text'))
            else:
                print("")

        except exceptions.NoSuchElementException:
            continue
    sum_dict.update({"attribute": attribute_dict})
    with open(path, 'w') as outfile:
        json.dump(sum_dict, outfile, ensure_ascii=False)

    print("bitti mutlu ol")
    print(path)
