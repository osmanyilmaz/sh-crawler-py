# sahibinden-crawler

## Installation
* Install virtualenv
* create virtualenv with `virtualenv sahibinden-crawler`
* install requirements with `pip install -r requirements.txt`
* install chromedriver to `/usr/local/bin` (optional)
* run main.py by `python main.py`
