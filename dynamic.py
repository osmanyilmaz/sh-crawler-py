from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common import exceptions
import json
from datetime import datetime

import os
start=datetime.now()

def write_file(path, sum_dict):
    with open(path + ".json", 'w') as outfile:
        json.dump(sum_dict, outfile, ensure_ascii=False)


def create_json():
    sum_dict = dict()
    feature_dict = dict()
    attribute_dict = dict()
    try:
        accordions = driver.find_elements_by_xpath("//div[@class='accordion']//dl")  # accordion part
    except:
        accordions = []
        pass
    for node in accordions:
        try:
            feat_title = node.find_element_by_xpath("dt//a")
            key = feat_title.text
            feature_dict.update({key: []})
            features = node.find_elements_by_xpath("..//dl//dd//div")
            for feature in features:
                text = feature.find_element_by_tag_name("label")
                if text.get_attribute('textContent') == "Tümü":
                    continue
                feature_dict[key].append(text.get_attribute('textContent'))
        except exceptions.NoSuchElementException:
            continue

    elem = driver.find_elements_by_xpath("//*[@id='categoryMeta']/div/table[1]/tbody/tr")  # element part
    for el in elem:
        key = el.find_element_by_class_name('first')
        if key.text == "Kategori" or key.text == "Adres" or key.text == "Fiyat" or key.text == "Arama Kelimesi":
            continue

        attribute_dict.update({key.text: []})

        base = el.find_element_by_xpath("td[@class='second']")
        values = base.find_elements_by_xpath('.//option')  # first value tümü
        input3 = base.find_elements_by_xpath(".//input[@class='searchTextBox numericInput']")
        text_input = base.find_elements_by_xpath(".//input[@class='inputDefault wordInput']")
        if len(text_input) == 1:  # text field or not
            continue
        elif len(input3) == 2:  # range or not
            continue
        elif values:
            # all_options = values.find_elements_by_tag_name("option")

            for al in values:
                if al.get_attribute('text') == "Tümü":
                    continue
                attribute_dict[key.text].append(al.get_attribute('text'))
        else:
            continue

    sum_dict.update({"features": feature_dict})  # total dict
    sum_dict.update({"attribute": attribute_dict})
    return sum_dict



select_driver = '//*[@id="categoryMeta"]/div/div/table/tbody/tr/td[2]/div/div/div/div/div[2]/div/div/div/ul/li'
newSelect_driver = '//*[@id="categoryMeta"]/div/div[1]/table/tbody/tr/td[2]/div/div/div/div/div[3]/div/div/div/ul/li'
newSelect2_driver = '//*[@id="categoryMeta"]/div/div[1]/table/tbody/tr/td[2]/div/div/div[1]/div/div[4]/div/div/div[1]/ul/li'

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
CHROME_PATH = os.path.join(ROOT_DIR, 'chromedriver')
driver = webdriver.Chrome(CHROME_PATH)

url = "https://www.sahibinden.com/arama/detayli?category=3518"
path = '/Users/minimac/Desktop/sahibinden2/emlak'
driver.get(url)

try:

    select = driver.find_elements_by_xpath(select_driver) #katman 1
    for index in range(len(select)):   #katman 1 in alt katmanları yani katman2
        if select[index].get_attribute('class') != 'default' and select[index].text != "Devremülk":
            temp1 = path
            path = path + "-" + select[index].text.lower().replace(" ", "_").replace("/", "").replace("\\", "").replace(",", "").replace("&", "").replace("__", "_")
            select[index].click()
            try:
                newSelect = WebDriverWait(driver, 10).until(
                    EC.presence_of_all_elements_located((By.XPATH, newSelect_driver))
                )
            except:


                write_file(path, create_json())

                select = driver.find_elements_by_xpath(select_driver)
                path = temp1

                continue

            for i in range(len(newSelect)): #katman 2 nin alt katmanları katman3
                if newSelect[i].text == "Tümü": #pass the default
                    continue
                temp2 = path
                path = path + "-" +newSelect[i].text.lower().replace(" ", "_").replace("/", "").replace("\\", "").replace(",", "").replace("&", "").replace("__", "_")
                newSelect[i].click()

                try:
                    newSelect2 = WebDriverWait(driver, 10).until(
                        EC.presence_of_all_elements_located((By.XPATH, newSelect2_driver))
                    )
                except:
                    write_file(path, create_json())
                    newSelect = driver.find_elements_by_xpath(newSelect_driver)

                    path = temp2
                    continue


                for j in range(len(newSelect2)): #katman 4
                    if newSelect2[j].text == "Tümü":  # pass the default
                        continue
                    temp3 = path

                    path = path + "-" + newSelect2[j].text.lower().replace(" ", "_").replace("/", "").replace("\\", "").replace(",", "").replace("&", "").replace("__", "_")

                    newSelect2 = driver.find_elements_by_xpath(newSelect2_driver) #tıklayacağı yeri buluyo
                    newSelect2[j].click() #tıklıyo
                    write_file(path, create_json()) #içerde yazacağı yeri buluyo
                    newSelect2 = driver.find_elements_by_xpath(newSelect2_driver) #tıklamadan önce ki değere geri dönüyo

                    path = temp3

                newSelect = driver.find_elements_by_xpath(newSelect_driver)
                path = temp2

            #print(element.find_element_by_tag_name('span').get_attribute('href'), element.text)
            select = driver.find_elements_by_xpath(select_driver)
            path = temp1
finally:
    driver.quit()
    print(datetime.now() - start)

